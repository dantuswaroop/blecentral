package com.realwear.blecentral

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.realwear.blecentral.ui.main.MainFragment
import com.realwear.keyboard.DeviceFinderFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, DeviceFinderFragment.newInstance())
                .commitNow()
        }
    }
}