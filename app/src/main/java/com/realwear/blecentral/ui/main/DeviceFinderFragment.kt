/*
 * RealWear Development Software, Source Code and Object Code.
 * (C) RealWear, Inc. All rights reserved.
 *
 * Contact info@realwear.com for further information about the use of this code.
 *
 * Filename: DeviceFinderFragment.kt
 * Class: DeviceFinderFragment.kt
 * Author: swaroop.dantu
 *
 */

package com.realwear.keyboard

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bluetoothlechat.scan.DeviceScanAdapter
import com.realwear.blecentral.R
import com.realwear.bluetoothle.ble.blescan.DeviceScanViewModel
import com.realwear.bluetoothle.ble.blescan.DeviceScanViewState

class DeviceFinderFragment : Fragment() {

    companion object {
        fun newInstance() = DeviceFinderFragment()
    }

    private lateinit var editText: EditText
    private lateinit var errorAction: Button
    private lateinit var errorMessage: TextView
    private lateinit var chatConfirmContainer: LinearLayout
    private lateinit var errorLayout: LinearLayout
    private lateinit var noDevices: TextView
    private lateinit var deviceList: RecyclerView
    private lateinit var scanning: LinearLayout
    private lateinit var viewModel: DeviceScanViewModel

    private val deviceScanAdapter by lazy {
        DeviceScanAdapter(onDeviceSelected)
    }

    private val viewStateObserver = Observer<DeviceScanViewState> { state ->
        when (state) {
            is DeviceScanViewState.ActiveScan -> showLoading()
            is DeviceScanViewState.ScanResults -> showResults(state.scanResults)
            is DeviceScanViewState.Error -> showError(state.message)
            is DeviceScanViewState.AdvertisementNotSupported -> showAdvertisingError()
        }
    }

    private val onDeviceSelected: (BluetoothDevice) -> Unit = { device ->
//        ChatServer.setCurrentChatConnection(device)
        viewModel.connectToDevice(device, {
            activity?.runOnUiThread {


                if (it) {
                    editText.visibility = View.VISIBLE
                    Toast.makeText(requireContext(), "Connected", Toast.LENGTH_SHORT).show()
                } else {
                    editText.visibility = View.GONE
                    Toast.makeText(requireContext(), "Disconnected", Toast.LENGTH_SHORT).show()
                }
                editText.setText("");
            }
        }, {
            activity?.runOnUiThread {
                val text = editText.text
                text.append(String(it))
                editText.text = text
            }
        })
        // navigate back to chat fragment
//        findNavController().popBackStack()
//        requireActivity().finish()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(DeviceScanViewModel::class.java)

        val inflate = inflater.inflate(R.layout.fragment_device_scan, container, false)
//        val devAddr = getString(R.string.your_device_address) + ChatServer.getYourDeviceAddress()
//        yourDeviceAddr.text = devAddr

        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scanning = view.findViewById(R.id.scanning) as LinearLayout
        errorLayout = view.findViewById(R.id.error) as LinearLayout
        chatConfirmContainer = view.findViewById(R.id.chat_confirm_container) as LinearLayout
        deviceList = view.findViewById(R.id.device_list) as RecyclerView
        noDevices = view.findViewById<TextView>(R.id.no_devices) as TextView
        errorMessage = view.findViewById<TextView>(R.id.error_message) as TextView
        errorAction = view.findViewById<Button>(R.id.error_action) as Button
        editText = view.findViewById<EditText>(R.id.edit_text) as EditText
        val view = view.findViewById<EditText>(R.id.your_device_addr) as TextView
        view.setOnClickListener {
            viewModel.sendWriteMessage("This is sample text")
        }

        deviceList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = deviceScanAdapter
        }

        viewModel.viewState.observe(viewLifecycleOwner, viewStateObserver)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.startScan()
    }

    private fun showLoading() {
        Log.d("TAG", "showLoading")
        scanning.visibility = View.VISIBLE

        deviceList.visibility = View.GONE
        noDevices.visibility = View.GONE
        errorLayout.visibility = View.GONE
        chatConfirmContainer.visibility = View.GONE
    }

    private fun showResults(scanResults: Map<String, BluetoothDevice>) {
        if (scanResults.isNotEmpty()) {
            deviceList.visibility = View.VISIBLE
            deviceScanAdapter.updateItems(scanResults.values.toList())

            scanning.visibility = View.GONE
            noDevices.visibility = View.GONE
            errorLayout.visibility = View.GONE
            chatConfirmContainer.visibility = View.GONE
        } else {
            showNoDevices()
        }
    }

    private fun showNoDevices() {
        noDevices.visibility = View.VISIBLE

        deviceList.visibility = View.GONE
        scanning.visibility = View.GONE
        errorLayout.visibility = View.GONE
        chatConfirmContainer.visibility = View.GONE
    }

    private fun showError(message: String) {
        Log.d("TAG", "showError: ")
        errorLayout.visibility = View.VISIBLE
        errorMessage.text = message

        // hide the action button if one is not provided
        errorAction.visibility = View.GONE
        scanning.visibility = View.GONE
        noDevices.visibility = View.GONE
        chatConfirmContainer.visibility = View.GONE
    }

    private fun showAdvertisingError() {
        showError("BLE advertising is not supported on this device")
    }

}