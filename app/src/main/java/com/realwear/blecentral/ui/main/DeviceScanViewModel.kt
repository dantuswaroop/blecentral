/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.realwear.bluetoothle.ble.blescan

import android.app.Application
import android.bluetooth.*
import android.bluetooth.le.*
import android.os.Handler
import android.os.ParcelUuid
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.blecentral.ui.main.BATTERY_LEVEL_UUID
import com.realwear.blecentral.ui.main.HEART_RATE_CONTROL_POINT_UUID
import com.realwear.blecentral.ui.main.SERVICE_UUID
import java.util.*


private const val TAG = "DeviceScanViewModel"
// 30 second scan period
private const val SCAN_PERIOD = 30000L

class DeviceScanViewModel(app: Application) : AndroidViewModel(app) {

    private lateinit var onNotification: (keyCode: ByteArray) -> Unit
    private lateinit var connectionStatus: (connected: Boolean) -> Unit
    private var writeCharacterstic: BluetoothGattCharacteristic? = null
    private var readCharacterstic: BluetoothGattCharacteristic? = null
    private lateinit var gatt: BluetoothGatt
    private var gattClient: BluetoothGatt? = null
    private lateinit var gattClientCallback: GattClientCallback

    // LiveData for sending the view state to the DeviceScanFragment
    private val _viewState = MutableLiveData<DeviceScanViewState>()
    val viewState = _viewState as LiveData<DeviceScanViewState>

    // String key is the address of the bluetooth device
    private val scanResults = mutableMapOf<String, BluetoothDevice>()

    // BluetoothAdapter should never be null since BLE is required per
    // the <uses-feature> tag in the AndroidManifest.xml
    private val adapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    // This property will be null if bluetooth is not enabled
    private var scanner: BluetoothLeScanner? = null

    private var scanCallback: DeviceScanCallback? = null
    private val scanFilters: List<ScanFilter>
    private val scanSettings: ScanSettings

    init {
        // Setup scan filters and settings
        scanFilters = buildScanFilters()
        scanSettings = buildScanSettings()

        // Start a scan for BLE devices
        startScan()
    }

    override fun onCleared() {
        super.onCleared()
        stopScanning()
    }

    fun startScan() {
        // If advertisement is not supported on this device then other devices will not be able to
        // discover and connect to it.
//        ChatServer.realwearIDMap.clear()
        if (!adapter.isMultipleAdvertisementSupported) {
            _viewState.value = DeviceScanViewState.AdvertisementNotSupported
            return
        }

        if (scanCallback == null) {
            scanner = adapter.bluetoothLeScanner
            Log.d(TAG, "Start Scanning")
            // Update the UI to indicate an active scan is starting
            _viewState.value = DeviceScanViewState.ActiveScan

            // Stop scanning after the scan period
            Handler().postDelayed({ stopScanning() }, SCAN_PERIOD)

            // Kick off a new scan
            scanCallback = DeviceScanCallback()
            scanner?.startScan(scanFilters, scanSettings, scanCallback)
        } else {
            Log.d(TAG, "Already scanning")
        }
    }

    private fun stopScanning() {
        Log.d(TAG, "Stopping Scanning")
        scanner?.stopScan(scanCallback)
        scanCallback = null
        // return the current results
        _viewState.value = DeviceScanViewState.ScanResults(scanResults)
    }

    /**
     * Return a List of [ScanFilter] objects to filter by Service UUID.
     */
    private fun buildScanFilters(): List<ScanFilter> {
        val builder = ScanFilter.Builder()
        // Comment out the below line to see all BLE devices around you
        builder.setServiceUuid(ParcelUuid(SERVICE_UUID))
        val filter = builder.build()
        return listOf(filter)
    }

    /**
     * Return a [ScanSettings] object set to use low power (to preserve battery life).
     */
    private fun buildScanSettings(): ScanSettings {
        return ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
            .build()
    }

    fun connectToDevice(device: BluetoothDevice, connectionStatus : (connected : Boolean) -> Unit,
    onNotification : (keyCode : ByteArray) -> Unit) {
        this@DeviceScanViewModel.connectionStatus = connectionStatus
        this@DeviceScanViewModel.onNotification = onNotification
        gattClientCallback = GattClientCallback()
        gattClient = device.connectGatt(getApplication(), false, gattClientCallback)
    }

    fun sendWriteMessage(s: String) {
        writeCharacterstic?.let { characteristic ->
            characteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE

            val messageBytes = s.toByteArray(Charsets.UTF_8)
            characteristic.value = messageBytes
            gatt?.let { it ->
                val success = it.writeCharacteristic(writeCharacterstic)
//                Timber.d("onServicesDiscovered: message send: $success")
//                if (success) {
//                    _messages.postValue(Message.LocalMessage(message))
//                }
            } ?: run {
//                Timber.d("sendMessage: no gatt connection to send a message with")
            }

//            characteristic.value = messageBytes
//            gatt?.let {
//                var success = it.writeCharacteristic(messageCharacteristic)
//                val startTime = System.currentTimeMillis()
//                while (!success) {
//                    success = it.writeCharacteristic(messageCharacteristic)
//                    val timeToBreak = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime)
//                    if (timeToBreak >= 2) {
//                        break
//                    }
//                }
//                Timber.d("onServicesDiscovered: message send: $success")
//                if (success) {
//                    _messages.postValue(Message.LocalMessage(message))
//                }
//            } ?: run {
//                Timber.d("sendMessage: no gatt connection to send a message with")
//            }
        }
    }

    private inner class GattClientCallback : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            val isSuccess = status == BluetoothGatt.GATT_SUCCESS
            val isConnected = newState == BluetoothProfile.STATE_CONNECTED
            Log.d(TAG, "onConnectionStateChange: Client $gatt  success: $isSuccess connected: $isConnected")
            // try to send a message to the other device as a test
            if (isSuccess && isConnected) {
                // discover services
//                val realWearID = realwearIDMap[gatt.device.address]
//                if (realWearID != null) {
//                    SharedPrefs.put(AppKeys.CONNECTED_BT_ID, realWearID)
//                }
                connectionStatus(true)
                gatt.discoverServices()
            } else {
                connectionStatus(false)
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, status)

        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            onNotification(characteristic!!.value)
        }

        override fun onServicesDiscovered(discoveredGatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(discoveredGatt, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "onServicesDiscovered: Have gatt $discoveredGatt")
                gatt = discoveredGatt
                val service = discoveredGatt.getService(SERVICE_UUID)
                readCharacterstic = service.getCharacteristic(BATTERY_LEVEL_UUID)
                writeCharacterstic = service.getCharacteristic(HEART_RATE_CONTROL_POINT_UUID)
//                sendMessage("ACK:"+discoveredGatt.device.address)

                gatt.setCharacteristicNotification(readCharacterstic, true)

                val uuid: UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
                val descriptor: BluetoothGattDescriptor = readCharacterstic!!.getDescriptor(uuid)
                descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                gatt.writeDescriptor(descriptor)
            }
        }
    }


    /**
     * Custom ScanCallback object - adds found devices to list on success, displays error on failure.
     */
    private inner class DeviceScanCallback : ScanCallback() {
        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)
            for (item in results) {
                val serviceData = item.scanRecord!!.manufacturerSpecificData
                println("device address " + serviceData.hashCode())

                item.device?.let { device ->
                    scanResults[device.address] = device
                }
            }
            _viewState.value = DeviceScanViewState.ScanResults(scanResults)
        }

        override fun onScanResult(
            callbackType: Int,
            result: ScanResult
        ) {
            super.onScanResult(callbackType, result)
//            val string = String(result.scanRecord!!.manufacturerSpecificData[1])
//            ChatServer.realwearIDMap[result.device.address] = string
//            val checkForReconnection = ChatServer.checkForReconnection(result)
//            if (checkForReconnection) {
//                stopScanning()
//                _viewState.value = DeviceScanViewState.DeviceConnected
//            }
            result.device?.let { device ->
                scanResults[device.address] = device
            }
            _viewState.value = DeviceScanViewState.ScanResults(scanResults)
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            // Send error state to the fragment to display
            val errorMessage = "Scan failed with error: $errorCode"
            _viewState.value = DeviceScanViewState.Error(errorMessage)
        }
    }


}